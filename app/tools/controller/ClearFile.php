<?php

declare(strict_types=1);

namespace app\tools\controller;

use app\model\Share;
use app\model\ShareFiles;
use think\facade\Filesystem;
use think\facade\Log;
use think\Request;

class ClearFile
{
    public function do()
    {

        $now_time = time();

        $list_share = Share::where('create_time', '<', $now_time - 86400 * 10)->where('clear_time', 0)->select();

        foreach ($list_share as  $model_share) {
            
            Log::debug('清空数据：' . $model_share->build_download_save_name);

            if (Filesystem::disk('safe')->fileExists($model_share->build_download_save_name)) {
                Filesystem::disk('safe')->delete($model_share->build_download_save_name);
            }

            $list_files = ShareFiles::where('share_id', $model_share->id)->select();


            foreach ($list_files as $model_files) {
                if (Filesystem::disk('safe')->fileExists($model_files->save_name)) {
                    Filesystem::disk('safe')->delete($model_files->save_name);
                }
                $model_files->clear_time = time();
                $model_files->save();
            }

            $model_share->clear_time = time();
            $model_share->status = 1;
            $model_share->save();
        }
    }
}
