<?php

declare(strict_types=1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Share extends Model
{
  //

  public function files()
  {
    return $this->hasMany(ShareFiles::class,'share_id');
  }

  public function getExpireDateAttr()
  {

    $date = date('Y-m-d H:i:s', $this->getAttr('expire_time'));

    return $date;
  }

  public function getTotalSizeFormatAttr()
  {
    return format_size($this->getData('total_size'));
  }

  public function getExpireTimeAttr()
  {
    $exipre = $this->getData('expire');
    return $this->getOrigin('create_time') + $exipre;
  }

  public function getReadUrlAttr()
  {
    return url('Index/read', ['uid' => $this->getData('uid')], false, true);
  }
}
