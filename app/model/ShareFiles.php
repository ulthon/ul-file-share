<?php

declare(strict_types=1);

namespace app\model;

use think\facade\App;
use think\Model;

/**
 * @mixin \think\Model
 */
class ShareFiles extends Model
{

  public static function build_safe_path($save_name)
  {
    $root_dir = App::getRootPath();

    $safe_dir = '/safe/';

    return $root_dir . $safe_dir . $save_name;
  }

  public function getFileSizeFormatAttr()
  {
    $value = $this->getData('file_size');
    return format_size($value);
  }
  //
}
