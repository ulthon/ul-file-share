# ul-file-share

#### 介绍
在线上传文件分享,开源的在线文件发送网站.

#### 主要功能

- 支持游客模式
- 支持多文件上传
- 支持垃圾文件清理
- 支持密码验证
- 支持下载次数限制
- 支持自动过期
- 支持后台审核文件

#### 效果

[![RievRg.png](https://z3.ax1x.com/2021/06/20/RievRg.png)](https://imgtu.com/i/RievRg)
[![RimSMj.png](https://z3.ax1x.com/2021/06/20/RimSMj.png)](https://imgtu.com/i/RimSMj)


#### 演示站点

该站点主要是内部使用,不承诺任何服务责任.

http://file-share.ulthon.com

#### 安装教程

```
1.安装
git clone https://gitee.com/ulthon/ul-file-share.git
2.进入目录
cd ul-file-share/
3.安装依赖
composer install
4.初始化数据库(默认将使用sqlite,请确保扩展正常)
php think migrate:run
php think seed:run
5.使用内置服务器
php think run -p 8010
6.访问前台
127.0.0.1:8010/index.php/index
7.访问后台
127.0.0.1:8010/index.php/admin
```


#### 版权

木兰协议2.0

#### 其他

目前只完成了核心功能,完全可以自用,如果有感兴趣的可以留言,我会继续推进项目

项目使用了user_hub用户中心.https://gitee.com/ulthon/user_hub
