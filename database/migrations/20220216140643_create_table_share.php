<?php

use app\common\ColumnFormat;
use think\migration\Migrator;
use think\migration\db\Column;

class CreateTableShare extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('share')
            ->setComment('分享表')
            ->addColumn(ColumnFormat::timestamp('create_time'))
            ->addColumn(ColumnFormat::timestamp('update_time'))
            ->addColumn(ColumnFormat::timestamp('delete_time'))
            ->addColumn(ColumnFormat::stringNormal('uid'))
            ->addColumn(ColumnFormat::stringNormal('password'))
            ->addColumn(ColumnFormat::integer('times'))
            ->addColumn(ColumnFormat::integer('times_last')->setComment('剩余次数'))
            ->addColumn(ColumnFormat::integer('expire'))
            ->addColumn(ColumnFormat::integer('total_size'))
            ->addColumn(ColumnFormat::stringNormal('build_download_save_name')->setComment('合成的下载包的存储路径'))
            ->addColumn(ColumnFormat::integer('user_id'))
            ->addColumn(ColumnFormat::integerTypeStatus('status'))
            ->addColumn(ColumnFormat::integer('times_download'))
            ->addColumn(ColumnFormat::integer('visit'))
            ->addColumn(ColumnFormat::integer('clear_time')->setComment('清空数据时间'))
            ->create();
    }
}
