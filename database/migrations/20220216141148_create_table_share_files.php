<?php

use app\common\ColumnFormat;
use think\migration\Migrator;
use think\migration\db\Column;

class CreateTableShareFiles extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('share_files')
        ->addColumn(ColumnFormat::timestamp('create_time'))
            ->addColumn(ColumnFormat::timestamp('update_time'))
            ->addColumn(ColumnFormat::timestamp('delete_time'))
            ->addColumn(ColumnFormat::timestamp('clear_time')->setComment('清除时间'))
            ->addColumn(ColumnFormat::stringNormal('save_name')->setComment('文件存储地址'))
            ->addColumn(ColumnFormat::stringNormal('file_name')->setComment('文件名'))
            ->addColumn(ColumnFormat::stringNormal('mime_type')->setComment('mime_type类型'))
            ->addColumn(ColumnFormat::integer('file_size')->setComment('文件大小'))
            ->addColumn(ColumnFormat::stringShort('ext_name')->setComment('扩展名'))
            ->addColumn(ColumnFormat::stringMd5('file_md5'))
            ->addColumn(ColumnFormat::stringNormal('file_sha1'))
            ->addColumn(ColumnFormat::integer('share_id'))
            ->create();
    }
}
